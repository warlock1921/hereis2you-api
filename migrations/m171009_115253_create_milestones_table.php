<?php

use yii\db\Migration;

/**
 * Handles the creation of table `milestones`.
 */
class m171009_115253_create_milestones_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
			$tableOptions = null;
			if ($this->db->driverName === 'mysql') {
					$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
			}
			
			$this->createTable('milestones', [
				'id' => $this->primaryKey(),
				'user_id' => $this->string()->notNull(),
				'milestone_name' => $this->text()->notNull(),
				'milestone_details' => $this->text()->notNull(),
				'milestone_date' => $this->string()->notNull(),
				'notify_friends' => $this->boolean()->notNull(),
				'created_at' => $this->integer()->notNull(),
				'updated_at' => $this->integer()->notNull(),
			], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('milestones');
    }
}
