<?php

use yii\db\Migration;

/**
 * Handles the creation of table `friends`.
 */
class m171006_104355_create_friends_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
			$tableOptions = null;
			if ($this->db->driverName === 'mysql') {
					$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
			}
			
			$this->createTable('friends', [
				'id' => $this->primaryKey(),
				'fb_id' => $this->string()->notNull(),
				'name' => $this->string()->notNull(),
				'friend_id' => $this->string()->notNull(),
				'image_url' => $this->text()->notNull(),
				'birth_date' => $this->string()->notNull(),
				'created_at' => $this->integer()->notNull(),
				'updated_at' => $this->integer()->notNull(),
			], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('friends');
    }
}
