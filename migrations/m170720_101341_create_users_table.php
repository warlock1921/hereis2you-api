<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170720_101341_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
		 


    public function up()
    {
				$tableOptions = null;
	      if ($this->db->driverName === 'mysql') {
	          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	      }
				
				$this->createTable('user', [
					'id' => $this->primaryKey(),
					'fb_id' => $this->string()->notNull()->unique(),
					'user_name' => $this->string()->notNull(),
					'image_url' => $this->text()->notNull(),
					'email' => $this->string()->notNull()->unique(),
					'birth_date' => $this->string()->notNull(),
					'gender' => $this->string()->notNull(),
					'device_type' => $this->string()->notNull(),
					'device_token' => $this->text()->notNull(),
					'stripe_user_id' => $this->string()->notNull(),
					'friends' =>$this->text()->notNull(),
					'created_at' => $this->integer()->notNull(),
          'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
