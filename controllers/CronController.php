<?php

namespace app\controllers;
use yii\rest\ActiveController;
use app\models\User;
use app\models\Friends;
use app\models\Milestones;
use \yii\db\Query;

use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

class CronController extends \yii\web\Controller
{		
	
		// To send birthday notification
		public function send_bday_notification($device_token, $bday_user)
		{
			$apiKey = FCM_SERVER_KEY;
			$client = new Client();
			$client->setApiKey($apiKey);
			$client->injectHttpClient(new \GuzzleHttp\Client());
			$note = new Notification($bday_user.' has birthday today !!', 'Click here to send your wishes.');
			$note->setIcon('notification_icon_resource_name')
					->setColor('#ffffff')
					->setBadge(1);

			$message = new Message();
			if($device_token) {
				$message->addRecipient(new Device($device_token));
				$message->setNotification($note)
						->setData(array('type' => 'milestone'));
				$response = $client->send($message);
				if($response->getStatusCode() == 200)
				{
					echo "Birthday notification sent successfully !!!";
				}
			}
			
		}
		
		public function send_milestone_notification($device_token, $data)
		{
			$apiKey = FCM_SERVER_KEY;
			$client = new Client();
			$client->setApiKey($apiKey);
			$client->injectHttpClient(new \GuzzleHttp\Client());
			$note = new Notification($data->milestone_name, $data->milestone_details);
			$note->setIcon('notification_icon_resource_name')
					->setColor('#ffffff')
					->setBadge(1);

			$message = new Message();
			if($device_token) {
				$message->addRecipient(new Device($device_token));
				$message->setNotification($note)
						->setData(array('type' => 'milestone'));
				$response = $client->send($message);
				if($response->getStatusCode() == 200)
				{
					echo "Notification sent successfully !!!";
				}
			}
			
		}
		
		// To get device_token from and user id
		public function get_device_token($id) {
			$token = User::find()->select('device_token')->where(['fb_id' => $id ])->one(); 
			return $token['device_token'];
		}
		
    public function actionBirthdayNotification()
    {  
      // return $this->render('index');
			$today = date("d/m");  
			$today= "01/21";
			
			// get users that have bithday today
			$users = User::find()->select(['user_name', 'fb_id', 'friends'])->where(['LIKE', 'birth_date', $today.'%', false])->all();
			if(count($users) > 0 )		 
			{
				foreach ($users as $user) 
				{	
					$friends = json_decode($user['friends']);
					foreach ($friends as $friend) 
					{
						$device_token =$this->get_device_token($friend->id);
						$this->send_bday_notification($device_token, $user['user_name']);
					}
				}
			}		 		
    }
		
		public function actionMilestoneNotification()
    {  
      // return $this->render('index');
			$today = date("d/m"); 
						
			// get Milestones for today
			$milestones = Milestones::find()->where(['LIKE', 'milestone_date', $today.'%', false])->all();
			
			if(count($milestones) > 0 )		 
			{
				foreach ($milestones as $milestone) 
				{	
					if($milestone->notify_friends) {
						$friends = Friends::find()->select('fb_id')->where(['friend_id' => $milestone->user_id ])->all();
						foreach ($friends as $friend) {
							$device_token = $this->get_device_token($friend['fb_id']);
							$this->send_milestone_notification($device_token, $milestone);
						}
					}
				}
			}		 		
    }
		
		

}
