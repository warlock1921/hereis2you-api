<?php
namespace app\controllers;
use yii\rest\ActiveController;
use app\models\User;
use app\models\Friends;
use \yii\db\Query;

if (isset($_SERVER['HTTP_ORIGIN'])) {
	 header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	 header('Access-Control-Allow-Credentials: true');
	 header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class UserController extends \yii\web\Controller
{		
		public $enableCsrfValidation = false;
		
		function beforeAction($action)
		{		
				\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
				
				$request = \Yii::$app->request;
				
				$api = '';
		
				//get
				if ($request->isGet)  { // the request method is GET 
					$api = $request->getHeaders()->get('Authorization');
				}
				if ($request->isPost) { // the request method is POST 
					$api = $request->getHeaders()->get('Authorization');
				}
		
				$key = \Yii::$app->params['apiKey'];
				
				
				//check for a valid api
				if($key != $api){
					echo json_encode(array("message"=>"INVALID API KEY","success"=>false));
					return false;
				}	
				return true;
		}
		
		public function actionIndex()
		{
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON; 
			
			return array( 
				"success"=>true,
				 "message"=>"API called successfully."
		 );
				// return $this->render('index');
		}
	
		public function actionCreate()
		{
		  \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			

			$request = \yii::$app->request; //request object
			if ($request->isPost) 
			{
				$data = json_decode(file_get_contents('php://input'),true);

				$user = User::find()->where(['fb_id' => $data['id'] ])->one();
				  
				if(empty($user)) {
				   $user = new User();	
				}
								
				$user->fb_id = $data['id'];
				$user->user_name = $data['name'];
				$user->image_url = $data['picture']['data']['url'];
				$user->email = $data['email'];
				$user->birth_date = isset($data['birthday']) ? $data['birthday'] : "Not Available";
				$user->gender = $data['gender'];
        $user->stripe_user_id = isset($data['stripe_user_id']) ? $data['stripe_user_id'] : "Not Available";
				$user->device_type = isset($data['device_type']) ? $data['device_type'] : "Not Available";
				$user->device_token = isset($data['device_token']) ? $data['device_token'] : "Not Available";
				$user->friends = json_encode($data['friends']['data']);
				$user->stripe_user_id = isset($data['stripe_user_id']) ? $data['stripe_user_id'] : "Not Available";
				$user->created_at = strtotime(date("Y-m-d H:i:s"));	
				$user->updated_at = strtotime(date("Y-m-d H:i:s"));	
				

			  if($user->save())
				{	
					if(!$user->isNewRecord) {
						Friends::deleteAll('friend_id ='.$data['id']);
					}			
					$friends_data = $data['friends']['data'];
					foreach ($friends_data as $value) {
						$friend = new Friends();
						$friend->fb_id = $value['id'];
						$friend->name = $value['name'];
						$friend->friend_id = $data['id'];
						$friend->image_url = $value['picture']['data']['url'];
						$friend->birth_date = isset($value['birthday']) ? $value['birthday'] : "Not Available";
						$friend->created_at = strtotime(date("Y-m-d H:i:s"));	
						$friend->updated_at = strtotime(date("Y-m-d H:i:s"));	
						$friend->save();
					}			
				  return array('status' => true, 'data'=> 'User record is successfully saved');					 			
			  }			
			  else
				{				
			    return array('status' => false, 'data'=> $user->getErrors());   				
			  }
			}
		}
		
		public function actionGet($id)		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

			// find user by facebook id
			$user = User::find()->where(['fb_id' => $id ])->one();   

			if(count($user) > 0 )		 
			{		 		
				return array('status' => true, 'data'=> $user); 		 
			}		 
			else		 
			{		 
				return array('status'=>false,'data'=> 'No User Found');		 
			}
		 
		}
		
		
		public function actionUpdate($id)
		{
		  \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			
			$request = \yii::$app->request; //request object
			if ($request->isPost) 
			{		
				$data = json_decode(file_get_contents('php://input'),true);
				// find user by facebook id
				$user = User::find()->where(['fb_id' => $id ])->one();   

			 if(count($user) > 0 )		 
			 {
				 	if(isset($data['id'])) {
						$user->fb_id = $data['id'];
					}
				 	if(isset($data['name'])) {
						$user->user_name = $data['name'];
					}
					if(isset($data['picture'])) {
						$user->image_url = $data['picture']['data']['url'];
					}
					if(isset($data['email'])) {
						$user->email = $data['email'];
					}
					if(isset($data['birthday'])) {
						$user->birth_date = $data['birthday'];
					}
					if(isset($data['gender'])) {
						$user->gender = $data['gender'];
					}
					if(isset($data['friends'])) {
						$user->friends = json_encode($data['friends']['data']);
					}
					if(isset($data['stripe_user_id'])) {
						$user->stripe_user_id =  $data['stripe_user_id'];
					}
					if(isset($data['device_type'])) {
						$user->device_type = $data['device_type'];
					}
					if(isset($data['device_token'])) {
						 $user->device_token = $data['device_token'];
					}
					if(isset($data['name'])) {
						$user->user_name = $data['name'];
					}
					if(isset($data['name'])) {
						$user->user_name = $data['name'];
					}
					$user->updated_at = strtotime(date("Y-m-d H:i:s"));	
 					
	 			  if($user->update())
	 				{	
						if(isset($data['friends'])) {
							Friends::deleteAll('friend_id ='.$data['id']);
							$friends_data = $data['friends']['data'];
							foreach ($friends_data as $value) {
								$friend = new Friends();
								$friend->fb_id = $value['id'];
								$friend->name = $value['name'];
								$friend->friend_id = $data['id'];
								$friend->image_url = $value['picture']['data']['url'];
								$friend->birth_date = isset($value['birthday']) ? $value['birthday'] : "Not Available";
								$friend->created_at = strtotime(date("Y-m-d H:i:s"));	
								$friend->updated_at = strtotime(date("Y-m-d H:i:s"));	
								$friend->save();
							}
						}							
	 			  	return array('status' => true, 'data'=> 'User record is successfully updated');					 			
	 			  }			
	 			  else
	 				{				
	 			    return array('status' => false, 'data'=> $user->getErrors());   				
	 			  }
			 }			
			}
		}
		
		public function actionFriends($id)		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

			// find friends using friend id
			$friends = Friends::find()->where(['friend_id' => $id ])->all();   
			if(count($friends) > 0 )		 
			{		 		
				return array('status' => true, 'data'=> $friends); 		 
			}		 
			else		 
			{		 
				return array('status'=>false,'data'=> 'No User Found');		 
			}
		}
		
		public function actionBirthdays($id)		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			$today = date("m/d");
			$birthdays = Friends::find()->where(['friend_id' => $id])->andWhere(['LIKE', 'birth_date', $today.'%', false])->all(); 
			if(count($birthdays) > 0 )		 
			{		 		
				return array('status' => true, 'data'=> $birthdays); 		 
			}		 
			else		 
			{		 
				return array('status'=>false,'data'=> 'No Birthday Found For Today');		 
			}
		}
		
		
		public function actionPicture($id)		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

			// find user by facebook id
			$user_image = User::find()->select('image_url')->where(['fb_id' => $id ])->one();
			if(count($user_image) > 0 )		 
			{		 		
				return array('status' => true, 'data'=> $user_image->image_url); 		 
			}		 
			else		 
			{		 
				return array('status'=>false,'data'=> 'No Picture found Found');		 
			}
		}

}

