<?php
namespace app\controllers;
use yii\rest\ActiveController;
use app\models\User;
use app\models\Friends;
use app\models\Milestones;
use \yii\db\Query;

if (isset($_SERVER['HTTP_ORIGIN'])) {
	 header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	 header('Access-Control-Allow-Credentials: true');
	 header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}


class MilestonesController extends \yii\web\Controller
{		
		public $enableCsrfValidation = false;
		
		function beforeAction($action)
		{		
				\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
				
				$request = \Yii::$app->request;
				
				$api = '';
		
				//get
				if ($request->isGet)  { // the request method is GET 
					$api = $request->getHeaders()->get('Authorization');
				}
				if ($request->isPost) { // the request method is POST 
					$api = $request->getHeaders()->get('Authorization');
				}
		
				$key = \Yii::$app->params['apiKey'];
				
				
				//check for a valid api
				if($key != $api){
					echo json_encode(array("message"=>"INVALID API KEY","success"=>false));
					return false;
				}	
				return true;
		}
	
		public function actionIndex()
		{
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON; 
			
			return array( 
				"success"=>true,
				 "message"=>"API called successfully."
		 );
				// return $this->render('index');
		}
		
		
		public function actionAdd()
		{
		  \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			
			$request = \yii::$app->request; //request object
			if ($request->isPost) 
			{
				$data = json_decode(file_get_contents('php://input'),true);
			  
				$milestone = new Milestones();	
				$milestone->user_id = $data['user_id'];
				$milestone->milestone_name = $data['name'];
				$milestone->milestone_details = isset($data['details']) ? $data['details'] : NA;
				$milestone->milestone_date = isset($data['date']) ? $data['date'] : NA;
				$milestone->notify_friends = $data['friends'] ? $data['friends'] : 0;
				$milestone->created_at = strtotime(date("Y-m-d H:i:s"));	
				$milestone->updated_at = strtotime(date("Y-m-d H:i:s"));	
				
			  if($milestone->save())
				{			
				  return array('status' => true, 'data'=> 'Milestone is successfully saved');					 			
			  }			
			  else
				{				
			    return array('status' => false, 'data'=> $milestone->getErrors());   				
			  }
			}
		}
		
		
		public function actionGet($id)		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

			// find milestones using user_id
			$milestones = Milestones::find()->where(['user_id' => $id ])->all();   
			if(count($milestones) > 0 )		 
			{		 		
				return array('status' => true, 'data'=> $milestones); 		 
			}		 
			else		 
			{		 
				return array('status'=>false,'data'=> 'No Milestone Found');		 
			}
		}
		
		public function actionToday($id)		 
		{	
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			
			$today = date("d/m"); 
			// find friends of current user 
			$friends = Friends::find()->select('fb_id')->where(['friend_id' => $id ])->all(); 
			if(count($friends) > 0 )		 
			{
				$ids = [];
				foreach ($friends as $friend) 
				{	
					array_push($ids, $friend->fb_id);
				}
				
				$milestones = Milestones::find()->where(['user_id' => $ids, 'notify_friends' => 1])->andWhere(['LIKE', 'milestone_date', $today.'%', false])->with('user')->all();
				$image_url_array = [];	
				foreach ($milestones as $mile) {
					$image_url_array[$mile->user_id] = $mile->user->image_url;
				}
				return array('status'=>true,'data'=> $milestones, 'images' => $image_url_array); 
			}
			else		 
			{		 
				return array('status'=>false,'data'=> 'No Milestone Found');		 
			}
		}
		
		public function actionDeleteMilestone()		 
		{	 
			\Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
			
			$data = json_decode(file_get_contents('php://input'),true);
			$milestones = Milestones::find()->where(['id' => $data['id']])->one()->delete();
			if($milestones) {
				return array('status'=>true,'data'=> 'Milestone Deleted successfully.');	
			}
			return array('status'=>false,'data'=> 'Problem in deleting milestone.');	
		}

}
