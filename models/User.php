<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $fb_id
 * @property string $user_name
 * @property string $image_url
 * @property string $email
 * @property string $birth_date
 * @property string $gender
 * @property string $device_type
 * @property string $device_token
 * @property string $stripe_user_id
 * @property string $friends
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fb_id', 'user_name', 'image_url', 'email', 'birth_date', 'gender', 'device_type', 'device_token', 'stripe_user_id', 'friends', 'created_at', 'updated_at'], 'required'],
            [['image_url', 'device_token', 'friends'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['fb_id', 'user_name', 'email', 'birth_date', 'gender', 'device_type', 'stripe_user_id'], 'string', 'max' => 255],
            [['fb_id'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fb_id' => 'Fb ID',
            'user_name' => 'User Name',
            'image_url' => 'Image Url',
            'email' => 'Email',
            'birth_date' => 'Birth Date',
            'gender' => 'Gender',
            'device_type' => 'Device Type',
            'device_token' => 'Device Token',
            'stripe_user_id' => 'Stripe User ID',
            'friends' => 'Friends',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
