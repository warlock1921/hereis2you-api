<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "friends".
 *
 * @property integer $id
 * @property string $fb_id
 * @property string $name
 * @property string $friend_id
 * @property string $image_url
 * @property string $birth_date
 * @property integer $created_at
 * @property integer $updated_at
 */
class Friends extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friends';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fb_id', 'name', 'friend_id', 'image_url', 'birth_date', 'created_at', 'updated_at'], 'required'],
            [['image_url'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['fb_id', 'name', 'friend_id', 'birth_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fb_id' => 'Fb ID',
            'name' => 'Name',
            'friend_id' => 'Friend ID',
            'image_url' => 'Image Url',
            'birth_date' => 'Birth Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
