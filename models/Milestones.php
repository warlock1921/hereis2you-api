<?php

namespace app\models;
use app\models\User;

use Yii;

/**
 * This is the model class for table "milestones".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $milestone_name
 * @property string $milestone_details
 * @property string $milestone_date
 * @property integer $notify_friends
 * @property integer $created_at
 * @property integer $updated_at
 */
class Milestones extends \yii\db\ActiveRecord
{
		public $user_image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'milestone_name', 'milestone_details', 'milestone_date', 'notify_friends', 'created_at', 'updated_at'], 'required'],
            [['milestone_name', 'milestone_details'], 'string'],
            [['notify_friends', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'milestone_date'], 'string', 'max' => 255],
						['user_image', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'milestone_name' => 'Milestone Name',
            'milestone_details' => 'Milestone Details',
            'milestone_date' => 'Milestone Date',
            'notify_friends' => 'Notify Friends',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
		
		/**
     * @inheritdoc
     */
    public function getUser()
    {
			return $this->hasOne(User::className(),[ 'fb_id' => 'user_id']);
    }
}
